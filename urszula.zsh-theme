################################################################################
# Filename: urszula.zsh-theme
# Purpose:  zsh prompt. Last cmd success/fail indicator. Git.
# Author:   john@webarch.coop
# Link:     https://coderwall.com/p/j3jyia/my-zsh-prompt-looks-good-in-solarized
# Link:     https://stackoverflow.com/a/12935606/7222080
# Link:     https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html
# Caveats:  Assumes Oh-my-zsh. Could easily be modified to be less tied to a zsh framework.
# Created:  2022-10-13
# Modified:
################################################################################

# %h - history, as accessed through exclamation mark.
# %T - time, 24h (%t is 12h)
# %? - last exit code (hopefully 0)
# %~ - pwd, e.g. ~/bin
# %# - shell prompt (% for user, # for root)

[[ $ZSH_EVAL_CONTEXT =~ :file$ ]] && sourced=1 || sourced=0
if [[ $sourced -eq 0 ]]; then
    f=$(basename $0)
    cp $0 $HOME/bin/include
    rm $HOME/.oh-my-zsh/custom/themes/$f
    ln -s $HOME/bin/include/$f $HOME/.oh-my-zsh/custom/themes/$f
    printf "\'%s\' zsh prompt installed successfully.\n" "${f%.zsh-theme}"
    printf "Edit %s and set ZSH_THEME to \'%s\'.\n" "$HOME/.zshrc" "${f%.zsh-theme}"
    exit 0
fi

# https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html
setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' stagedstr 'M'
zstyle ':vcs_info:*' unstagedstr 'M'
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' actionformats '%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
zstyle ':vcs_info:*' formats \
  '%F{5}[%F{2}%b%F{5}] %F{2}%c%F{3}%u%f'
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
zstyle ':vcs_info:*' enable git
+vi-git-untracked() {
  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
  [[ $(git ls-files --other --directory --exclude-standard | sed q | wc -l | tr -d ' ') == 1 ]] ; then
  hook_com[unstaged]+='%F{1}??%f'
  fi
}

autoload -U colors && colors

_prmptcmd() {
  EXIT=$?
  if [[ $EXIT -gt 0 ]]; then
    PROMPT='😿 %F{230}%?%F{reset} %F{red}✯ %F{reset} %F{230}%n%F{244}@%m%F{reset} %F{red}✯ %F{reset} %F{230}%~%F{reset} ${vcs_info_msg_0_}%F{230}%#%F{reset} '
  else
    PROMPT='%F{230}%n%F{245}@%m%F{reset} %F{red}✯ %F{reset} %F{230}%~%F{reset} ${vcs_info_msg_0_}%F{230}%#%F{reset} '
  fi
}

precmd_functions=( vcs_info _prmptcmd )
RPROMPT='%D{%a %d %b} %F{red}✯%F{reset} %D{%H:%M:%S}'
PS2=''
