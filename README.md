[_metadata_:Directory]:- "~/Development/zsh/prompt"
[_metadata_:TimeMachine]:- "Included"
[_metadata_:Git]:- "Yes"
[_metadata_:Git-origin]:- "https://git.coop/johnn/zsh-prompt"

# Development/zsh/prompt

This is the development directory for my zsh prompt.

The prompt indicates that the last command failed by displaying a sad cat (and the return code).

Apart from that it does all the usual stuff you'd expect, including git.

# To do

Create `bash` version to use on TrueNAS and Debian VMs.

# Cheatsheet

Install the prompt in ~/bin/include and link to it from Oh-my-zsh's custom theme's directory:

    ./urszula.zsh-theme

# Licence

Copyright (c) 2022 [John Niven](mailto:john@webarch.coop)

Licensed under the GPL, v3.
